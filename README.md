# Chrome Serial App - Arduino

This app is based on a simple slider that, when dragged, causes the 13th Pin on Arduino to toggle. There is a feedback signal given back from the Arduino that toggles the Lamp Image in the App. Pin 13 on the Arduino toggles when the slider is changed in the Chrome App.

To mimic what the Arduino is doing on pin 13 the chrome log turns on and off just like it would on the Arduino pin 13.

The port filters out Bluetooth and only looks for COM strings, this worked on Windows XP and Chrome 27 and was demoed on Windows 7 and Chrome 29.

## Enable the experimental API in your browser. 

Go to Chrome://flags, find "Experimental Extension APIs", click its "Enable" link, and restart Chrome. From now on, unless you return to that page and disable experimental APIs, you'll be able to run extensions and apps that use experimental APIs.

## Install Chrome App

Go to chrome://extensions then click "Load unpacked extension...". Then click on Launch to get it working.

### APIs

* [Serial API](http://developer.chrome.com/trunk/apps/app.hardware.html#serial)
